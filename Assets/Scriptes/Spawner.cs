using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    private float _time;
    public float spawnTime;
    public Transform destroyPoint;
    public GameObject spawnObject;
    public float speed;
    GameObject[] objcts;

    void Update()
    {
        var step = speed * Time.deltaTime;
        _time = _time + Time.deltaTime;

        if (_time >= spawnTime)
        {
            GameObject objct = Instantiate(spawnObject, transform.position, Quaternion.identity);

            _time = 0;
        }

         objcts = GameObject.FindGameObjectsWithTag("Object");
        for (int i = 0; i < objcts.Length; i++)
        {
            objcts[i].transform.position = Vector3.MoveTowards(objcts[i].transform.position, destroyPoint.position, step);

            if (objcts[i].transform.position == destroyPoint.position)
            {
                Destroy(objcts[i]);
            }
        }
    }
}
